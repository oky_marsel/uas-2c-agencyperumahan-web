/*
 Navicat Premium Data Transfer

 Source Server         : okymarsel
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : localhost:3306
 Source Schema         : agensi

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 17/05/2020 11:15:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cp
-- ----------------------------
DROP TABLE IF EXISTS `cp`;
CREATE TABLE `cp`  (
  `id_cp` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `agency` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `alamat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `telp` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_cp`) USING BTREE,
  UNIQUE INDEX `agency`(`agency`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cp
-- ----------------------------
INSERT INTO `cp` VALUES (1, 'Hartoyo Mahdi', 'Gajayana Residence', 'Jl. Merbabu No.5 Kota Kediri', '087666566455', 'hartoyo@gajayana.com');
INSERT INTO `cp` VALUES (2, 'Reni Anggraeni', 'Greenland', 'Jl. Merapi No.55 Paron - Kediri', '085765444456', 'reni@greenland.com');
INSERT INTO `cp` VALUES (3, 'Rudi Tabuti', 'Syariah Cluster', 'Jl. Suhat No.01 - Jombang', '089789678567', 'rudi.syariah@residence.com');
INSERT INTO `cp` VALUES (4, 'Yaudah Iya', 'Merbabu Estate', 'Jl. Kita Cukup Disi - Aja', '123456789012', 'okedeh@baibai.com');
INSERT INTO `cp` VALUES (5, 'heni', 'Harmony Regency', 'Jl. Lingkar Maskumambang - Jakarta Barat', '089798567455', 'heni@harmony.com');

-- ----------------------------
-- Table structure for perum
-- ----------------------------
DROP TABLE IF EXISTS `perum`;
CREATE TABLE `perum`  (
  `id_perum` int(11) NOT NULL AUTO_INCREMENT,
  `id_tipe` int(11) NULL DEFAULT NULL,
  `id_cp` int(11) NULL DEFAULT NULL,
  `deskripsi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `images` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_perum`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of perum
-- ----------------------------
INSERT INTO `perum` VALUES (1, 2, 1, 'Luas 200x200meter, 2 Kamar tidur, Garasi, Dapur', 'perum1.jpg');
INSERT INTO `perum` VALUES (2, 1, 2, 'Luas 150x200meter, 2 Kamar Tidur, dapur, kamar mandi', 'perum2.jpg');
INSERT INTO `perum` VALUES (3, 3, 3, 'Luas 250x200meter, 3 kamar tidur, garasi, dapur, 2 kamar mandi', 'perum3.jpg');
INSERT INTO `perum` VALUES (4, 6, 4, 'Luas 150x150meter, 2 kamar tidur, 1 kamar mandi', 'perum4.jpg');
INSERT INTO `perum` VALUES (5, 5, 3, 'Luas 250x200meter, 3 kamar tidur, garasi, dapur, 2 kamar mandi', 'perum5.jpg');
INSERT INTO `perum` VALUES (6, 4, 2, 'Luas 250x250meter, 4 kamar tidur, garasi, dapur, 2 kamar mandi', 'perum6.jpg');

-- ----------------------------
-- Table structure for tipe
-- ----------------------------
DROP TABLE IF EXISTS `tipe`;
CREATE TABLE `tipe`  (
  `id_tipe` int(11) NOT NULL AUTO_INCREMENT,
  `nama_tipe` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `harga` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_tipe`) USING BTREE,
  UNIQUE INDEX `nama_tipe`(`nama_tipe`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tipe
-- ----------------------------
INSERT INTO `tipe` VALUES (1, '36', '40000000');
INSERT INTO `tipe` VALUES (2, '45', '75000000');
INSERT INTO `tipe` VALUES (3, '54', '150000000');
INSERT INTO `tipe` VALUES (4, '70', '250000000');
INSERT INTO `tipe` VALUES (5, '120', '80000000');
INSERT INTO `tipe` VALUES (6, '21/24', '200000000');
INSERT INTO `tipe` VALUES (7, 'A105', '50000000');

-- ----------------------------
-- Table structure for video
-- ----------------------------
DROP TABLE IF EXISTS `video`;
CREATE TABLE `video`  (
  `id_vid` int(11) NOT NULL AUTO_INCREMENT,
  `nama_niv` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_vid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of video
-- ----------------------------
INSERT INTO `video` VALUES (1, 'gajayana_residence.mp4');
INSERT INTO `video` VALUES (2, 'greenland.mp4');
INSERT INTO `video` VALUES (3, 'harmoni_residence.mp4');
INSERT INTO `video` VALUES (4, 'syariah_cluster.mp4');

SET FOREIGN_KEY_CHECKS = 1;
