

<?php 
    include("koneksi.php");
    if (!isset($_GET['id'])) {
		header('Location:detail.php');
    }
    $id = $_GET['id'];
    $sql = "SELECT p.id_perum, t.harga, t.nama_tipe, 
    p.deskripsi, p.images, c.alamat,
    c.telp, c.agency, c.nama, c.telp, c.email
    FROM perum p, tipe t, cp c
    WHERE p.id_perum=$id AND p.id_tipe=t.id_tipe AND p.id_cp=c.id_cp";
	$query = mysqli_query($db, $sql);
    $detil = mysqli_fetch_assoc($query);
    if (mysqli_num_rows($query) < 1) {
      die("data tidak ditemukan...");
    }
  
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>UAS 2C Agency Perumahan</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
  </head>

  <body>

    <header>
      <div class="collapse bg-dark" id="navbarHeader">
        <div class="container">
          <div class="row">
            <div class="col-sm-8 col-md-7 py-4">
              <h4 class="text-white">About</h4>
              <p class="text-muted">Aplikasi mengelola data perumahan tiap agency untuk dipromosikan ke calon pembeli.</p>
            </div>
            <div class="col-sm-4 offset-md-1 py-4">
              <h4 class="text-white">Contact</h4>
              <ul class="list-unstyled">
                <li><a href="#" class="text-white">Follow on Twitter</a></li>
                <li><a href="#" class="text-white">Like on Facebook</a></li>
                <li><a href="#" class="text-white">Email me</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="navbar navbar-dark bg-dark box-shadow">
        <div class="container d-flex justify-content-between">
          <a href="index.php" class="navbar-brand d-flex align-items-center">
            <strong>Agency Perumahan</strong>
          </a>
          <a class="nav-link" href="perum.php">Perumahan</a>
          <a class="nav-link" href="cp.php">Contact Person</a>
          <a class="nav-link" href="video.php">Videos</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        </div>
      </div>
    </header>

    <main role="main">
      <div class="container">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel" >
                  <div class="x_title" >
                    <h4 class="text-center"><b > <?php echo $detil['agency'] ?></b> </h4>
                    </div>
                  <div class="x_content">

                    <div class="col-md-7 col-sm-7 ">
                      <div class="product-image">
                        <img src="../images/<?php echo $detil['images'] ?>" alt="..." />
                      </div>
                    </div>

                    <div class="col-md-5 col-sm-5 " style="border:0px solid #e5e5e5;">

                      <h1>Tipe : <?php echo $detil['nama_tipe'] ?></h1>

                      <p><?php echo $detil['deskripsi'] ?></p>
                      
                      
                      <div class="">
                        <div class="product_price">
                          <h1 class="price">Rp.<?php echo $detil['harga'] ?></h1>
                          
                        </div>
                      </div>
                      <br />
                      <br />
                      <div class="">
                        <p>Untuk Pemesanan Hubungi Kontak dibawah ini :</p>
                        <h3>Marketing : <?php echo $detil['nama'] ?> <i> (<?php echo $detil['telp'] ?>)</i>  <a href=""><?php echo $detil['email'] ?> </a></h3>
                        
                        
                      </div>
                      <br />

                      

                      

                      <!-- <div class="">
                        <button type="button" class="btn btn-default btn-lg">Add to Cart</button>
                        <button type="button" class="btn btn-default btn-lg">Add to Wishlist</button>
                      </div> -->

                      <div class="product_social">
                        <ul class="list-inline display-layout">
                          <li><a href="#"><i class="fa fa-facebook-square"></i></a>
                          </li>
                          <li><a href="#"><i class="fa fa-twitter-square"></i></a>
                          </li>
                          <li><a href="#"><i class="fa fa-envelope-square"></i></a>
                          </li>
                          <li><a href="#"><i class="fa fa-rss-square"></i></a>
                          </li>
                        </ul>
                      </div>

                    </div>


                    
                  </div>
                </div>
                <!-- <footer class="text-muted">
      <div class="container">
        <p class="float-right">
          <a href="#">Back to top</a>
        </p>
        <p>Copyright&copy;2020, Moh Misbahus Surur & Oky Juneva Marsel</p>
      </div>
    </footer> -->
              </div>
              </div>
    </main>

    

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>