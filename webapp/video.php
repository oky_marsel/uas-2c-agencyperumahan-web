<?php 
	include("koneksi.php");
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>UAS 2C Agency Perumahan</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">
  </head>

  <body>

    <header>
      <div class="collapse bg-dark " id="navbarHeader">
        <div class="container">
          <div class="row">
            <div class="col-sm-8 col-md-7 py-4">
              <h4 class="text-white">About</h4>
              <p class="text-muted">Aplikasi mengelola data perumahan tiap agency untuk dipromosikan ke calon pembeli.</p>
            </div>
            <div class="col-sm-4 offset-md-1 py-4">
              <h4 class="text-white">Contact</h4>
              <ul class="list-unstyled">
                <li><a href="#" class="text-white">Follow on Twitter</a></li>
                <li><a href="#" class="text-white">Like on Facebook</a></li>
                <li><a href="#" class="text-white">Email me</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="navbar navbar-dark bg-dark box-shadow ">
        <div class="container d-flex justify-content-between">
          <a href="index.php" class="navbar-brand d-flex align-items-center">
            <strong>Agency Perumahan</strong>
          </a>
          <a class="nav-link" href="perum.php">Perumahan</a>
          <a class="nav-link" href="cp.php">Contact Person</a>
          <a class="nav-link" href="video.php">Videos</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        </div>
      </div>
    </header>

    <main role="main">
    <h1><center>Video Preview Perumahan</center></h1>
      <div class="album py-5 bg-light">
        <div class="container">
          <div class="row">
            <div class="col-md-4">
                <div class="card mb-4 box-shadow">
                <video height="200px" controls>
		            <source src="../videos/greenland.mp4" type="video/mp4">
	            </video>
                <div class="card-body">
                    <h4 class="card-tect">GreenLand Residence</h4>
                  <p class="card-text">Jl. Merapi No.55 Paron - Kediri</p>
                </div>
              </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-4 box-shadow">
                <video height="200px" controls>
		            <source src="../videos/gajayana_residence.mp4" type="video/mp4">
	            </video>
                <div class="card-body">
                    <h4 class="card-tect">Gajayana Residence</h4>
                  <p class="card-text">Jl. Merbabu No.5 Kota Kediri</p>
                </div>
              </div>
            </div>
            <div class="col-md-4">
                <div class="card mb-4 box-shadow">
                <video height="200px" controls>
		            <source src="../videos/harmoni_residence.mp4" type="video/mp4">
	            </video>
                <div class="card-body">
                    <h4 class="card-tect">Harmoni Residence</h4>
                  <p class="card-text">Jl. Lingkar Maskumambang - Jakarta Barat</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    


    </main>

    <footer class="text-muted">
      <div class="container">
        <p class="float-right">
          <a href="#">Back to top</a>
        </p>
        <p>Copyright&copy;2020, Moh Misbahus Surur & Oky Juneva Marsel</p>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>
