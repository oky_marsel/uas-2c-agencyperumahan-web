<?php 
	include("koneksi.php");
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>UAS 2C Agency Perumahan</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">
  </head>

  <body>

    <header>
      <div class="collapse bg-dark" id="navbarHeader">
        <div class="container">
          <div class="row">
            <div class="col-sm-8 col-md-7 py-4">
              <h4 class="text-white">About</h4>
              <p class="text-muted">Aplikasi mengelola data perumahan tiap agency untuk dipromosikan ke calon pembeli.</p>
            </div>
            <div class="col-sm-4 offset-md-1 py-4">
              <h4 class="text-white">Contact</h4>
              <ul class="list-unstyled">
                <li><a href="#" class="text-white">Follow on Twitter</a></li>
                <li><a href="#" class="text-white">Like on Facebook</a></li>
                <li><a href="#" class="text-white">Email me</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="navbar navbar-dark bg-dark box-shadow">
        <div class="container d-flex justify-content-between">
          <a href="index.php" class="navbar-brand d-flex align-items-center">
            <strong>Agency Perumahan</strong>
          </a>
          <a class="nav-link" href="perum.php">Perumahan</a>
          <a class="nav-link" href="cp.php">Contact Person</a>
          <a class="nav-link" href="video.php">Videos</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        </div>
      </div>
    </header>

    <main role="main">
    <div class="container">
        <header>
            <h1><center>Daftar Kontak<center></h1>
        </header>
        <!-- MODAL INSERT -->
        <button  type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">Tambah Kontak</button>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambahkan Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="proses-data-cp.php" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="id_cp" class="col-form-label">ID Kontak :</label>
                                <input type="text" class="form-control" id="id_cp" name="id_cp">
                            </div>
                            <div class="form-group">
                                <label for="agency" class="col-form-label">Nama Agency :</label>
                                <input type="text" class="form-control" id="agency" name="agency" required>
                            </div>
                            <div class="form-group">
                                <label for="alamat" class="col-form-label">Alamat Agency :</label>
                                <textarea class="form-control" id="alamat" name="alamat" required></textarea>
                            </div>
                            <div class="form-group">
                                <label for="nama" class="col-form-label">Marketing :</label>
                                <input type="text" class="form-control" id="nama" name="nama" required>
                            </div>
                            <div class="input-group row">
                            <div class="form-group col-8 col-sm-6">
                                <label for="telp" class="col-form-label">No Telp :</label>
                                <input type="text" class="form-control" id="telp" name="telp" required>
                            </div>
                            <br>
                            <div class="form-group col-8 col-sm-6">
                                <label for="email" class="col-form-label">Email :</label>
                                <input type="text" class="form-control" id="email" name="email" required>
                            </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                <button type="submit" name="tambah" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MODAL  -->

        <table class="table table-bordered table-hover">
		    <thead>
			    <tr class="text-center">
				    <th>ID</th>
				    <th>Agency</th>
				    <th>Alamat</th>
            <th>Marketing</th>
            <th>No Telp</th>
            <th>Email</th>
            <th>Opsi</th>
			    </tr>
		    </thead>
            <tbody>
            <?php 
				    $sql = "SELECT * FROM cp
                    ORDER BY id_cp";
                    $query = mysqli_query($db,$sql);
                    while ($cp = mysqli_fetch_array($query)) {
                        ?>
                        <tr>
                            <td><?php echo $cp['id_cp'] ?></td>
                            <td><?php echo $cp['agency'] ?></td>
                            <td><?php echo $cp['alamat'] ?></td>
                            <td><?php echo $cp['nama'] ?></td>
                            <td><?php echo $cp['telp'] ?></td>
                            <td><?php echo $cp['email'] ?></td>
                            <td>
						    <div class='btn-group'>
                                <a href="edit_cp.php?id=<?php echo $cp['id_cp']?>">
                                    <button class="btn btn-xs btn-info tipsy-kiri-atas"><i class="icon-edit icon-white"></i>Edit</button>
                                </a>
                                <a href="hapus_cp.php?id=<?php echo $cp['id_cp']?>" title="Hapus Data Ini" onclick="return confirm('ANDA YAKIN AKAN MENGHAPUS DATA PENTING INI ... ?')">
                                    <button class="btn btn-xs btn-danger tipsy-kiri-atas"><i class="icon-remove icon-white"></i>Hapus</button>
                                </a> 
						    </div>
						</td>
                        </tr>
                    <?php
                    }
                ?>
            </tbody>
        </table>
    </div>
    </main>

    <footer class="text-muted">
      <div class="container">
        <p class="float-right">
          <a href="#">Back to top</a>
        </p>
        <p>Copyright&copy;2020, Moh Misbahus Surur & Oky Juneva Marsel</p>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>