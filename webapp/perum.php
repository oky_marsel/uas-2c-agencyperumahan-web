<?php 
	include("koneksi.php");
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>UAS 2C Agency Perumahan</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">
  </head>

  <body>

    <header>
      <div class="collapse bg-dark" id="navbarHeader">
        <div class="container">
          <div class="row">
            <div class="col-sm-8 col-md-7 py-4">
              <h4 class="text-white">About</h4>
              <p class="text-muted">Aplikasi mengelola data perumahan tiap agency untuk dipromosikan ke calon pembeli.</p>
            </div>
            <div class="col-sm-4 offset-md-1 py-4">
              <h4 class="text-white">Contact</h4>
              <ul class="list-unstyled">
                <li><a href="#" class="text-white">Follow on Twitter</a></li>
                <li><a href="#" class="text-white">Like on Facebook</a></li>
                <li><a href="#" class="text-white">Email me</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="navbar navbar-dark bg-dark box-shadow">
        <div class="container d-flex justify-content-between">
          <a href="index.php" class="navbar-brand d-flex align-items-center">
            <strong>Agency Perumahan</strong>
          </a>
          <a class="nav-link" href="perum.php">Perumahan</a>
          <a class="nav-link" href="cp.php">Contact Person</a>
          <a class="nav-link" href="video.php">Videos</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        </div>
      </div>
    </header>

    <main role="main">
    <div class="container">
        <header>
            <h1><center>Daftar Perumahan<center></h1>
        </header>
        <!-- MODAL INSERT -->
        <button  type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal">Tambah Perumahan</button>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambahkan Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="proses-data-perum.php" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="id_perum" class="col-form-label">ID Perum :</label>
                                <input type="text" class="form-control" id="id_perum" name="id_perum" required>
                            </div>
                            <div class="form-group">
                                <label for="id_cp" class="col-form-label">Contact Person :</label>
                                <select class="form-control" name="id_cp" required>
                                    <?php while($row1 = mysqli_fetch_row($result1)):; ?>
                                    <option value="<?php echo $row1[0]; ?>"><?php echo $row1[1]; ?></option>
                                    <?php endwhile; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="id_tipe" class="col-form-label">ID Tipe :</label>
                                <select class="form-control" name="id_tipe" required>
                                    <?php while($row2 = mysqli_fetch_row($result2)):; ?>
                                    <option value="<?php echo $row2[0]; ?>"><?php echo $row2[1]; ?></option>
                                    <?php endwhile; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="deskripsi" class="col-form-label">Deskripsi</label>
                                <textarea class="form-control" id="deskripsi" name="deskripsi" required></textarea>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Gambar</span>
                                </div>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="images" name="images" required>
                                    <label class="custom-file-label" for="images">Pilih Gambar</label>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                                <button type="submit" name="tambah" class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MODAL  -->
        <table class="table table-bordered table-hover">
		    <thead>
			    <tr>
				    <th>No</th>
				    <th>Agency</th>
				    <th>Tipe</th>
                    <th>Harga</th>
                    <th>Alamat</th>
                    <th>Telp</th>
                    <th>Deskripsi</th>
                    <th>Gambar</th>
                    <th>Opsi</th>
			    </tr>
		    </thead>
            <tbody>
            <?php 
				    $sql = "SELECT p.id_perum, t.harga, t.nama_tipe, 
                    p.deskripsi, p.images, c.alamat,
                    c.telp, c.agency
                    FROM perum p, tipe t, cp c
                    WHERE p.id_tipe=t.id_tipe AND p.id_cp=c.id_cp
                    ORDER BY p.id_perum";
                    $query = mysqli_query($db,$sql);
                    while ($perum = mysqli_fetch_array($query)) {
                        ?>
                        <tr>
                            <td><?php echo $perum['id_perum'] ?></td>
                            <td><?php echo $perum['agency'] ?></td>
                            <td><?php echo $perum['nama_tipe'] ?></td>
                            <td><?php echo $perum['harga'] ?></td>
                            <td><?php echo $perum['alamat'] ?></td>
                            <td><?php echo $perum['telp'] ?></td>
                            <td><?php echo $perum['deskripsi'] ?></td>
                            <td><img src='../images/<?php echo $perum['images'] ?>' width='100' height='50' /></td>
                            <td>
						              <div class="btn-group mr-2">
                                <a href="edit_perum.php?id=<?php echo $perum['id_perum']?>">
                                    <button class="btn btn-xs btn-info tipsy-kiri-atas"><i class="icon-edit icon-white"></i>Edit</button>
                                </a>
                                <a href="hapus_perum.php?id=<?php echo $perum['id_perum']?>" title="Hapus Data Ini" onclick="return confirm('ANDA YAKIN AKAN MENGHAPUS DATA PENTING INI ... ?')">
                                    <button class="btn btn-xs btn-danger tipsy-kiri-atas"><i class="icon-remove icon-white"></i>Hapus</button>
                                </a> 
						    </div>
						</td>
                        </tr>
                    <?php
                    }
                ?>
            </tbody>
        </table>
    </div>
    </main>

    <footer class="text-muted">
      <div class="container">
        <p class="float-right">
          <a href="#">Back to top</a>
        </p>
        <p>Copyright&copy;2020, Moh Misbahus Surur & Oky Juneva Marsel</p>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>